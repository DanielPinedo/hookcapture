<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Hook
 * @property CI_Output $output
 * @property CI_Benchmark $benchmark
 */
class Hook extends CI_Controller
{
	function index()
	{
		$this->load->database();
		$this->output->enable_profiler(true);

		$arrayData = [];

		$arrayData[ 'COOKIE' ] = $_COOKIE;
		$arrayData[ 'ENV' ] = $_ENV;
		$arrayData[ 'FILES' ] = $_FILES;
		$arrayData[ 'GET' ] = $_GET;
		$arrayData[ 'POST' ] = $_POST;
		$arrayData[ 'REQUEST' ] = $_REQUEST;
		$arrayData[ 'SERVER' ] = $_SERVER;

		$this->db->query(
			'INSERT INTO hooks VALUES (' . $this->db->escape( json_encode($arrayData) ) . ')'
		);
	}
}